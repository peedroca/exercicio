﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;
using Nsf._2018.Modulo3.App.DB.Carrinho;
using Nsf._2018.Modulo3.App.DB.Pedido;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmPedidoCadastrar : UserControl
    {                
        int cod = 0;

        public frmPedidoCadastrar()
        {
            InitializeComponent();
            Produtos();
            Random rand = new Random();
            cod = rand.Next(111111, 999999);
        }

        private void btnEmitir_Click(object sender, EventArgs e)
        {
            

            PedidoDTO dto = new PedidoDTO();
            dto.nm_cliente = txtCliente.Text;
            dto.cpf = txtCpf.Text;
            
        }

        private void Produtos()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            ProdutoDTO dto = new ProdutoDTO();
            List<string> Produtos = new List<string>();

            foreach (ProdutoDTO produtos in db.Produtos(dto))
            {
                Produtos.Add(produtos.nm_produto.ToString());
            }

            cboProduto.DataSource = Produtos;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            CarrinhoDTO dto = new CarrinhoDTO();
            dto.qnt_produto = Convert.ToInt32(txtQuantidade.Text);
            dto.nm_produto = cboProduto.SelectedItem.ToString();
            dto.cod_compra = cod;

            CarrinhoBusiness business = new CarrinhoBusiness();
            business.AdicionarProduto(dto);

            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = business.ConsultarProdutos(dto);
        }
    }
}