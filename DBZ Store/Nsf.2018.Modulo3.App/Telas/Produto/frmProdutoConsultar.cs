﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;
using System.Text.RegularExpressions;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmProdutoConsultar : UserControl
    {
        public frmProdutoConsultar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Regex regexNmProduto = new Regex("^[a-zA-Z0-9 ç]{0,200}$");

            if (regexNmProduto.IsMatch(txtProduto.Text) == false)
                throw new ArgumentException("O nome não pode conter caracteres especiais como '. , ; @ # $' Ou letras acentuadas");

            ProdutoDTO dto = new ProdutoDTO();
            dto.nm_produto = txtProduto.Text;

            ProdutoBusiness business = new ProdutoBusiness();
            dgvProdutos.AutoGenerateColumns = false;
            dgvProdutos.DataSource = business.Consultar(dto);
        }
    }
}
