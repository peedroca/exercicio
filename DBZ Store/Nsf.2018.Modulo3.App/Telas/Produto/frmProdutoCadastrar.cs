﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.DB.Produto;
using System.Text.RegularExpressions;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmProdutoCadastrar : UserControl
    {
        public frmProdutoCadastrar()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPreco.Text.Contains(",") != true)
                    txtPreco.Text = $"{txtPreco.Text},00";

                Regex regexNmProduto = new Regex("^[a-zA-Z0-9 ç]{1,200}$");
                Regex regexVlProduto = new Regex("^[0-9]{1,},[0-9]{2}$");

                if (regexNmProduto.IsMatch(txtProduto.Text) == false)
                    throw new ArgumentException("Você deve digitar um nome. Sem usar caracteres especiais como '. , ; @ # $' Ou letras acentuadas");

                if (regexVlProduto.IsMatch(txtPreco.Text) == false)
                    throw new ArgumentException("Você deve digitar um preço. Que deve estar no formato '0,00'");

                ProdutoDTO dto = new ProdutoDTO();
                dto.nm_produto = txtProduto.Text;
                dto.vl_preco = Convert.ToDecimal(txtPreco.Text);

                ProdutoBusiness business = new ProdutoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Produto salvo com sucesso", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Erro!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void txtPreco_MouseClick(object sender, MouseEventArgs e)
        {
            txtPreco.Text = string.Empty;
        }
    }
}
