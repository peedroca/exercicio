﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Carrinho
{
    class CarrinhoDTO
    {
        public int qnt_produto { get; set; }
        public string nm_produto { get; set; }
        public int cod_compra { get; set; }
    }
}
