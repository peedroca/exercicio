﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Carrinho
{
    class CarrinhoDatabase
    {
        public void AdicionarProduto(CarrinhoDTO dto) {
            string script = @"INSERT INTO tb_carrinho(cod_compra, qnt_produto, nm_produto) VALUES(@cod_compra, @qnt_produto, @nm_produto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("cod_compra", dto.cod_compra));
            parms.Add(new MySqlParameter("qnt_produto", dto.qnt_produto));
            parms.Add(new MySqlParameter("nm_produto", dto.nm_produto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<CarrinhoDTO> ConsultarProdutos(CarrinhoDTO dto)
        {
            string script = @"SELECT qnt_produto, nm_produto FROM tb_carrinho WHERE cod_compra = @cod_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("cod_compra", dto.cod_compra));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CarrinhoDTO> DTO = new List<CarrinhoDTO>();
            while (reader.Read())
            {
                CarrinhoDTO Dto = new CarrinhoDTO();
                Dto.nm_produto = reader.GetString("nm_produto");
                Dto.qnt_produto = reader.GetInt32("qnt_produto");

                DTO.Add(Dto);
            }

            reader.Close();
            return DTO;
        }
    }
}
