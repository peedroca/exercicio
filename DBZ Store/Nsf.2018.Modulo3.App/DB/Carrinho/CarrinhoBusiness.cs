﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Carrinho
{
    class CarrinhoBusiness
    {
        public void AdicionarProduto(CarrinhoDTO dto)
        {
            CarrinhoDatabase db = new CarrinhoDatabase();
            db.AdicionarProduto(dto);
        }
        public List<CarrinhoDTO> ConsultarProdutos(CarrinhoDTO dto)
        {
            CarrinhoDatabase db = new CarrinhoDatabase();
            return db.ConsultarProdutos(dto);
        }
    }
}
