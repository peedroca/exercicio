﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoBusiness
    {
        public void Salvar(ProdutoDTO dto) {
            if (dto.nm_produto == string.Empty)
                throw new ArgumentException("Nome do produto é obrigatório!");

            ProdutoDatabase db = new ProdutoDatabase();
            db.Salvar(dto);
        }

        public List<ProdutoDTO> Consultar(ProdutoDTO dto) {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Consultar(dto);
        } 
    }
}
