﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Produto
{
    class ProdutoDatabase
    {
        public void Salvar(ProdutoDTO dto)
        {
            string script = @"INSERT INTO tb_produto(nm_produto, vl_preco) VALUES(@nm_produto, @vl_preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.nm_produto));
            parms.Add(new MySqlParameter("vl_preco", dto.vl_preco));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        List<MySqlParameter> parms = new List<MySqlParameter>();
        string script = string.Empty;

        public List<ProdutoDTO> Consultar(ProdutoDTO dto)
        {
            if (dto.nm_produto != string.Empty)
            {
                script = @"SELECT * FROM tb_produto WHERE nm_produto = @nm_produto";

                parms.Add(new MySqlParameter("nm_produto", dto.nm_produto));
            }
            else if (dto.nm_produto == string.Empty)
            {
                script = @"SELECT * FROM tb_produto";
            }

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> Produtos = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO produto = new ProdutoDTO();
                produto.id_produto = reader.GetInt32("id_produto");
                produto.nm_produto = reader.GetString("nm_produto");
                produto.vl_preco = reader.GetDecimal("vl_preco");

                Produtos.Add(produto);
            }

            reader.Close();
            return Produtos;
        }

        public List<ProdutoDTO> Produtos(ProdutoDTO dto)
        {
            script = @"SELECT nm_produto FROM tb_produto";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> Produtos = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO produto = new ProdutoDTO();
                produto.nm_produto = reader.GetString("nm_produto");

                Produtos.Add(produto);
            }

            reader.Close();
            return Produtos;
        }
    }
}