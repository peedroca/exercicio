﻿using MySql.Data.MySqlClient;
using Nsf._2018.Modulo3.App.DB.Base;
using Nsf._2018.Modulo3.App.DB.Carrinho;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class PedidoDatabase
    {
        public void Emitir(PedidoDTO dto)
        {
            string script = @"SELECT nm_produto, qnt_produto FROM tb_carrinho WHERE cod_compra = @cod_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("cod_compra", dto.cod));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CarrinhoDTO> DTO = new List<CarrinhoDTO>();
            while (reader.Read())
            {
                CarrinhoDTO Dto = new CarrinhoDTO();
                Dto.nm_produto = reader.GetString("nm_produto");
                Dto.qnt_produto = reader.GetInt32("qnt_produto");

                DTO.Add(Dto);
            }
            reader.Close();
           

        }
    }
}
